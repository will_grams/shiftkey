package com.shiftkey.codingchallenge.di

import com.shiftkey.codingchallenge.data.network.ShiftApi
import com.shiftkey.codingchallenge.util.Constants
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { okhttpClient() }
    single { retrofit(get()) }
    single { apiService(get()) }
}

fun apiService(retrofit: Retrofit): ShiftApi = retrofit.create(ShiftApi::class.java)

fun retrofit(okHttpClient: OkHttpClient): Retrofit =
    Retrofit.Builder()
        .baseUrl(Constants.API_BASE_PATH)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

fun okhttpClient(): OkHttpClient =
    OkHttpClient.Builder().build()