package com.shiftkey.codingchallenge.di

import com.shiftkey.codingchallenge.data.network.ShiftApi
import com.shiftkey.codingchallenge.data.repository.ShiftRepository
import org.koin.dsl.module

val repositoryModule = module {
    single { createRepository(get()) }
}

fun createRepository(
    shiftApi: ShiftApi
) : ShiftRepository = ShiftRepository(shiftApi)