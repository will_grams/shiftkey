package com.shiftkey.codingchallenge

import android.app.Application
import com.shiftkey.codingchallenge.di.networkModule
import com.shiftkey.codingchallenge.di.repositoryModule
import com.shiftkey.codingchallenge.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ShiftKeyApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@ShiftKeyApp)
            modules(listOf(networkModule, repositoryModule, viewModelModule))
        }
    }
}