package com.shiftkey.codingchallenge.ui.screen

import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.shiftkey.codingchallenge.model.Shift
import com.shiftkey.codingchallenge.ui.theme.ShiftKeyTheme
import com.shiftkey.codingchallenge.ui.theme.hexToJetpackColor

@Composable
fun InformationDialog(
    shift: Shift,
    onDismiss: () -> Unit,
    onNegativeClick: () -> Unit,
    onPositiveClick: () -> Unit,
) {
    ShiftKeyTheme {
        Column {
            Dialog(onDismissRequest = onDismiss) {
                Card(
                    modifier = Modifier
                        .fillMaxWidth(),
                    elevation = 8.dp,
                    shape = MaterialTheme.shapes.medium
                ) {
                    Column(modifier = Modifier.fillMaxWidth()) {
                        Row(
                            modifier = Modifier
                                .padding(8.dp)
                                .fillMaxWidth(),
                        ) {
                            Text(
                                text = "Shift Information"
                            )
                        }
                        Row(
                            modifier = Modifier
                                .padding(8.dp)
                                .fillMaxWidth(),
                        ) {
                            Column(
                                modifier = Modifier
                            ) {
                                Text(
                                    text = "Start: ${shift.normalized_start_date_time}",
                                    style = MaterialTheme.typography.h6
                                )
                                Text(
                                    text = "End: ${shift.normalized_end_date_time}",
                                    style = MaterialTheme.typography.h6
                                )
                                Text(
                                    text = shift.facility_type.name,
                                    color = hexToJetpackColor(shift.facility_type.color)
                                )
                                Text(
                                    text = shift.skill.name,
                                    color = hexToJetpackColor(shift.skill.color)
                                )
                                Text(
                                    text = shift.localized_specialty.specialty.name,
                                    color = hexToJetpackColor(
                                        shift.localized_specialty.specialty.color
                                    )
                                )
                                Text(
                                    modifier = Modifier.padding(start = 4.dp),
                                    text = shift.shift_kind
                                )

                                if (shift.premium_rate) {
                                    Text(
                                        modifier = Modifier.padding(start = 4.dp),
                                        text = "Premium Rate"
                                    )
                                }

                                if (shift.covid) {
                                    Text(
                                        modifier = Modifier.padding(start = 4.dp),
                                        text = "Covid"
                                    )
                                }

                                Text(
                                    text = "Distance: ${shift.within_distance} miles"
                                )
                            }
                        }
                        Row(
                            modifier = Modifier
                                .padding(8.dp)
                                .fillMaxWidth()
                            ,
                            horizontalArrangement = Arrangement.End,
                        ) {
                            TextButton(onClick = onPositiveClick) {
                                Text(
                                    text = "BOOK"
                                )
                            }
                            TextButton(onClick = onNegativeClick) {
                                Text(
                                    text = "CANCEL"
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}