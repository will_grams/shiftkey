package com.shiftkey.codingchallenge.ui.theme

import androidx.compose.ui.graphics.Color

val purple200 = Color(0xFFBB86FC)
val purple500 = Color(0xFF6200EE)
val purple700 = Color(0xFF3700B3)
val teal200 = Color(0xFF03DAC5)
val lightGray = Color(0xFFA9A9A9)
val darkGray = Color(0xFF333333)

fun hexToJetpackColor(hexString: String): Color {
    return Color(android.graphics.Color.parseColor(hexString))
}