package com.shiftkey.codingchallenge.ui.screen

import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.shiftkey.codingchallenge.data.repository.ShiftRepository
import com.shiftkey.codingchallenge.data.repository.paged.ShiftSource
import com.shiftkey.codingchallenge.model.Shift
import com.shiftkey.codingchallenge.util.Constants.ShiftWeekType
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate

class MainViewModel(
    private val shiftRepository: ShiftRepository
) : ViewModel() {
    fun shifts(
        startDate: LocalDate? = null,
        shiftWeekType: ShiftWeekType? = null,
        address: String,
        radius: Int? = null
    ): Flow<PagingData<Shift>> = Pager(PagingConfig(pageSize = 100)) {
        ShiftSource(
            shiftRepository,
            startDate = startDate,
            shiftWeekType = shiftWeekType,
            address = address,
            radius = radius
        )
    }.flow
}