package com.shiftkey.codingchallenge.ui.screen

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import com.shiftkey.codingchallenge.model.Shift
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate

@Composable
fun MainScreen(mainViewModel: MainViewModel, startDate: LocalDate, address: String) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "ShiftKey") }
            )
        },
        content = {
            Column(
                modifier = Modifier
                    .padding(start = 16.dp, top = 16.dp, end = 16.dp)
                    .fillMaxWidth(),
            ) {
                Text(
                    modifier = Modifier
                        .padding(bottom = 4.dp),
                    text = "Shifts near $address"
                )
                ShiftList(shifts = mainViewModel.shifts(startDate = startDate, address = address))
            }
        }
    )
}

@Composable
fun ShiftList(shifts: Flow<PagingData<Shift>>) {
    val lazyShiftItems = shifts.collectAsLazyPagingItems()

    LazyColumn {
        items(lazyShiftItems) { shift ->
            ShiftItem(shift = shift!!)
        }

        lazyShiftItems.apply {
            when {
                loadState.refresh is LoadState.Loading -> {
                    item { LoadingView(modifier = Modifier.fillParentMaxSize()) }
                }
                loadState.append is LoadState.Loading -> {
                    item { LoadingItem() }
                }
                loadState.refresh is LoadState.Error -> {
                    val e = lazyShiftItems.loadState.refresh as LoadState.Error
                    item {
                        ErrorItem(
                            message = e.error.localizedMessage!!,
                            modifier = Modifier.fillParentMaxSize(),
                            onClickRetry = { retry() }
                        )
                    }
                }
                loadState.append is LoadState.Error -> {
                    val e = lazyShiftItems.loadState.append as LoadState.Error
                    item {
                        ErrorItem(
                            message = e.error.localizedMessage!!,
                            onClickRetry = { retry() }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun ShiftItem(shift: Shift) {
    val showInformationDialog = remember { mutableStateOf(false) }
    val context = LocalContext.current
    if (showInformationDialog.value) {
        InformationDialog(
            shift = shift,
            onDismiss = {
                showInformationDialog.value = false
                Toast.makeText(context, "Dialog Dismissed", Toast.LENGTH_SHORT).show()
            },
            onNegativeClick = {
                showInformationDialog.value = false
                Toast.makeText(context, "Dialog Canceled", Toast.LENGTH_SHORT).show()
            },
            onPositiveClick = {
                showInformationDialog.value = false
                Toast.makeText(context, "Shift Booked", Toast.LENGTH_SHORT).show()
            }
        )
    }
    Row(
        modifier = Modifier
            .padding(start = 8.dp, top = 8.dp, end = 8.dp)
            .clip(MaterialTheme.shapes.small)
            .background(MaterialTheme.colors.secondary)
            .clickable {
                showInformationDialog.value = true
            }
            .padding(start = 8.dp, top = 8.dp, end = 8.dp, bottom = 8.dp)
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Column(
            modifier = Modifier
        ) {
            Text(
                modifier = Modifier,
                text = "Start: ${shift.normalized_start_date_time}",
                style = MaterialTheme.typography.h6
            )
            Text(
                modifier = Modifier,
                text = "End: ${shift.normalized_end_date_time}",
                style = MaterialTheme.typography.h6
            )
        }
        Column(modifier = Modifier) {
            Text(
                modifier = Modifier.padding(start = 4.dp),
                text = shift.shift_kind
            )

            if (shift.premium_rate) {
                Text(
                    modifier = Modifier.padding(start = 4.dp),
                    text = "Premium Rate"
                )
            }

            if (shift.covid) {
                Text(
                    modifier = Modifier.padding(start = 4.dp),
                    text = "Covid"
                )
            }
        }
    }
}
