package com.shiftkey.codingchallenge.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.shiftkey.codingchallenge.ui.screen.MainScreen
import com.shiftkey.codingchallenge.ui.screen.MainViewModel
import com.shiftkey.codingchallenge.ui.theme.ShiftKeyTheme
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.time.LocalDate

class MainActivity : ComponentActivity() {

    private val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ShiftKeyTheme {
                MainScreen(
                    mainViewModel = mainViewModel,
                    startDate = LocalDate.now(),
                    address = "Dallas, TX"
                )
            }
        }
    }
}