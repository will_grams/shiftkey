package com.shiftkey.codingchallenge.model

import java.io.Serializable

data class FacilityType(
    val color: String,
    val id: Int,
    val name: String
): Serializable