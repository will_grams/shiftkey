package com.shiftkey.codingchallenge.model

import java.io.Serializable

data class Specialty(
    val abbreviation: String,
    val color: String,
    val id: Int,
    val name: String
): Serializable