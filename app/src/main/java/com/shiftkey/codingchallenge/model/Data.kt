package com.shiftkey.codingchallenge.model

import java.io.Serializable

data class Data(
    val date: String,
    val shifts: List<Shift>
): Serializable