package com.shiftkey.codingchallenge.model

import java.io.Serializable

data class Meta(
    val lat: Double,
    val lng: Double
): Serializable