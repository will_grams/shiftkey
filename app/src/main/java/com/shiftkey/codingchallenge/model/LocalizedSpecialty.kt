package com.shiftkey.codingchallenge.model

import java.io.Serializable

data class LocalizedSpecialty(
    val abbreviation: String,
    val id: Int,
    val name: String,
    val specialty: Specialty,
    val specialty_id: Int,
    val state_id: Int
): Serializable