package com.shiftkey.codingchallenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ShiftResponse(
    @SerializedName("data")
    val shiftData: List<Data>,
    val links: List<Any>,
    val meta: Meta
): Serializable