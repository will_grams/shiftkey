package com.shiftkey.codingchallenge.util

object Constants {
    const val API_BASE_PATH = "https://staging-app.shiftkey.com/api/v2/"
    const val SHIFTS_API_PATH = "available_shifts"
    const val DATE_FORMAT = "yyyy-MM-dd"
    enum class ShiftWeekType {
        WEEK {
            override fun toString(): String {
                return "week"
            }
        },
        FOUR_DAY {
            override fun toString(): String {
                return "4day"
            }
        }
    }
}