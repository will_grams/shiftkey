package com.shiftkey.codingchallenge.data.repository

import com.shiftkey.codingchallenge.data.network.ShiftApi
import com.shiftkey.codingchallenge.model.Shift

class ShiftRepository(private val shiftApi: ShiftApi) {
    suspend fun getShifts(
        address: String,
        startDate: String? = null,
        type: String? = null,
        radius: String? = null
    ): List<Shift> {
        val shiftData = shiftApi.getShifts(address, startDate, type, radius).shiftData
        val shifts: MutableList<Shift> = mutableListOf()

        shiftData.forEach { data ->
            if (data.shifts.isNotEmpty()) {
                shifts.addAll(data.shifts)
            }
        }

        return shifts.toList()
    }
}