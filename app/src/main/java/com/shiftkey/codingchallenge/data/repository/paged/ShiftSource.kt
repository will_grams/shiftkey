package com.shiftkey.codingchallenge.data.repository.paged

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.shiftkey.codingchallenge.model.Shift
import com.shiftkey.codingchallenge.data.repository.ShiftRepository
import com.shiftkey.codingchallenge.util.Constants.DATE_FORMAT
import com.shiftkey.codingchallenge.util.Constants.ShiftWeekType
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class ShiftSource(
    private val shiftRepository: ShiftRepository,
    private val startDate: LocalDate? = null,
    private val shiftWeekType: ShiftWeekType? = null,
    private val address: String,
    private val radius: Int? = null
): PagingSource<Int, Shift>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Shift> {
        return try {
            val dateFormat = DateTimeFormatter.ofPattern(DATE_FORMAT)
            val nextPage = params.key ?: 1
            val getDate = startDate
                ?.plusWeeks(nextPage.toLong() - 1)
                ?.format(dateFormat) ?: run {
                LocalDate.now().plusWeeks(nextPage.toLong() - 1).format(dateFormat)
            }

            val shiftResponse = shiftRepository.getShifts(
                address = address,
                startDate = getDate,
                type = shiftWeekType?.toString() ?: ShiftWeekType.WEEK.toString(),
                radius = radius?.toString()
            )

            LoadResult.Page(
                data = shiftResponse,
                prevKey = if (nextPage == 1) null else nextPage - 1,
                nextKey = nextPage + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Shift>): Int? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(it)?.nextKey?.minus(1)
        }
    }
}