package com.shiftkey.codingchallenge.data.network

import com.shiftkey.codingchallenge.model.ShiftResponse
import com.shiftkey.codingchallenge.util.Constants.SHIFTS_API_PATH
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ShiftApi {
    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET(SHIFTS_API_PATH)
    suspend fun getShifts(
        @Query("address") address: String,
        @Query("start") startDate: String?,
        @Query("type") type: String?,
        @Query("radius") radius: String?
    ): ShiftResponse
}